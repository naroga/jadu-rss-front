import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs/Observable";
import * as moment from "moment"
@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    console.log(user);
    // console.log(Date(JSON.parse(localStorage.getItem('user')).token.expires_at);
    if (localStorage.getItem('user') === null || moment(user.token.expires_at) < moment()) {
      localStorage.removeItem('user');
      this.router.navigate(['/login'], { queryParams: { url: state.url }});
      return false;
    }
    return true;
  }
}
