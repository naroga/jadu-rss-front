import {Component, OnInit} from '@angular/core';
import {Headers, Http} from "@angular/http";
import {headers} from "../common/headers/headers";
import {base_url} from "../config";

@Component({
  selector: 'dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: [
    '../../assets/layouts/layout/css/layout.min.css',
    '../../assets/layouts/layout/css/themes/darkblue.min.css',
    '../../assets/layouts/layout/css/custom.css',
    './dashboard.component.scss'
  ]
})
export class DashbordComponent implements OnInit {

  channels: any[] = [];

  constructor(public http: Http) {
  }

  ngOnInit(): void {

    this.http.get(base_url + '/channels', {headers: headers}).subscribe(
      response => {
        this.channels = response.json();
      },
      error => {

      }
    );
  }

  addChannel(event, url): void
  {
    this.http.post(base_url + '/channel', {'url': url.value}, {headers: headers}).subscribe(
      response => {
        this.ngOnInit();
      }
    );

    event.preventDefault();


  }
}
