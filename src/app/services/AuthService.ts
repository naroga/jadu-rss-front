import {Injectable} from "@angular/core";
import {Http, Response} from "@angular/http";
import 'rxjs/add/operator/map'

@Injectable()
export class AuthService {
  constructor(private http: Http) {
  }

  login(username: string, password: string) {
    return this.http.post('http://localhost:8800/login', JSON.stringify({username: username, password: password}))
      .map((response: Response) => {
        const userToken = response.json().token;
        localStorage.setItem('user-token', userToken);
        return userToken;
      });
  }

  logout() {
    localStorage.removeItem('user-token');
  }
}
