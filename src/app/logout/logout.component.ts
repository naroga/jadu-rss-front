
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {updateUser} from "../common/headers/headers";

@Component({selector: 'logout', template: ''})
export class LogoutComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    updateUser();
    this.router.navigateByUrl('login', { queryParams: { url: '/dashboard' }});
  }
}

