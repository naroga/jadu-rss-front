import {Component} from '@angular/core';
import {user} from "../headers/headers";

@Component({
  selector: 'page-header',
  templateUrl: 'page-header.component.html',
  styleUrls: [
    '../../../assets/layouts/layout/css/layout.min.css',
    '../../../assets/layouts/layout/css/themes/darkblue.min.css',
    '../../../assets/layouts/layout/css/custom.css'
  ]
})
export class PageHeaderComponent {
  user: any = user;
}
