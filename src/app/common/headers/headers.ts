import {Headers} from "@angular/http";

export let headers = new Headers();
export let token = JSON.parse(localStorage.getItem('user'));
if (token) {
  headers.append('Content-Type', 'application/json');
  headers.append('Authorization', 'Bearer ' + token.token.token);
}
export let user = token ? token.user : null;

export const updateUser = function() {
  headers = new Headers();
  token = JSON.parse(localStorage.getItem('user'));
  if (token) {
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer ' + token.token.token);
  }
  user = token ? token.user : null;
};
