import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {RouterModule, Routes} from '@angular/router';
import {HttpModule} from '@angular/http';
import {DashbordComponent} from './dashboard/dashbord.component';
import {AuthGuard} from './security/auth.guard';
import {PageHeaderComponent} from './common/page-header/page-header.component';
import {LogoutComponent} from './logout/logout.component';

import {RegistrationComponent} from "./registration/registration.component";
import {ChannelComponent} from "./channel/channel.component";

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegistrationComponent},
  {path: 'dashboard', component: DashbordComponent, canActivate: [AuthGuard]},
  {path: 'logout', component: LogoutComponent, canActivate: [AuthGuard]},
  {path: 'channel/:channel', component: ChannelComponent, canActivate: [AuthGuard]},
  {path: '**', redirectTo: 'dashboard'}
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashbordComponent,
    PageHeaderComponent,
    LogoutComponent,
    RegistrationComponent,
    ChannelComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(routes),
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule {
}
