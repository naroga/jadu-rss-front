import {Component, OnInit} from "@angular/core";
import {Http} from "@angular/http";
import {base_url} from "../config";
import {headers} from "../common/headers/headers";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'channel',
  templateUrl: 'channel.component.html',
  styleUrls: [
    '../../assets/layouts/layout/css/layout.min.css',
    '../../assets/layouts/layout/css/themes/darkblue.min.css',
    '../../assets/layouts/layout/css/custom.css',
    '../../assets/global/css/plugins.css',
    '../../assets/global/css/components.css',
    '../../../node_modules/font-awesome/css/font-awesome.min.css',

  ]
})
export class ChannelComponent implements OnInit {

  public channel: any = null;

  constructor(private http: Http, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.http.get(base_url + '/channel/' + this.route.snapshot.paramMap.get('channel'), {headers: headers}).subscribe(
      response => {
        this.channel = response.json();
      });
  }

  unsubscribe(channel): void
  {
    this.http.delete(base_url + '/channel/' + channel.id, {headers: headers}).subscribe(
      response => {
        this.channel.subscribed = false;
      });
  }

  subscribe(channel): void
  {
    this.http.post(base_url + '/channel/' + channel.id, {}, {headers: headers}).subscribe(
      response => {
        this.channel.subscribed = true;
      });
  }
}
