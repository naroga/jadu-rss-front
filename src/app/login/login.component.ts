
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Headers, Http} from '@angular/http';
import {updateUser} from "../common/headers/headers";
import {base_url} from "../config";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  isLoading: boolean = false;
  error: string = '';
  url: string = '';

  constructor(private route: ActivatedRoute, private router: Router, public http: Http) {}

  ngOnInit(): void {
    this.url = this.route.snapshot.queryParams['url'] || '/dashboard';
  }

  login(username, password, rememberMe) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    event.preventDefault();
    this.isLoading = true;
    this.error = '';
    this.http.post(
      base_url + '/login',
      JSON.stringify({username: username, password: password, rememberMe: rememberMe === '1'}),
      {headers: headers})
    .subscribe(
      response => {
        localStorage.setItem('user', JSON.stringify(response.json()));
        this.isLoading = false;
        this.router.navigateByUrl(this.url);
        updateUser();
      },
      error => {
        this.error = error.json().error;
        this.isLoading = false;
      }
    );
  }
}
