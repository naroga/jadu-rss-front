
import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {Http} from "@angular/http";
import {base_url} from "../config";
import {updateUser} from "../common/headers/headers";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  error: string = '';
  url: string = '';

  constructor(private route: ActivatedRoute, private router: Router, public http: Http) {}

  ngOnInit(): void {
    this.url = this.route.snapshot.queryParams['url'] || '/dashboard';
  }

  createAccount = function(event, name, email, password) {
    event.preventDefault();
    this.http.post(base_url + '/register', {
      name: name,
      email: email,
      plainPassword: password,
    }).subscribe(
      response => {
        localStorage.setItem('user', JSON.stringify(response.json()));
        this.isLoading = false;
        this.router.navigateByUrl(this.url);
        updateUser();
      },
      error => {
        this.error = error.json().error;
      }
    );
  };
}
