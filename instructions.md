# Instructions

You will need to install the dependencies with `npm`:

```$ npm install```

You will then be ready to serve the project:

```$ npm start```

Then, navigate to http://localhost:4200/ to see the application frontend.
