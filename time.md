This is a breakdown of how time was spent in this project.

* skeleton configuration: 1h
* authorization/authentication (both user-based and access-token-based): 2h
* layout (from metronic files): 4h
* individual routes: 1h

TOTAL: 8h


